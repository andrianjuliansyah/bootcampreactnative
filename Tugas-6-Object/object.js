//SOAL 1 - ARRAY TO OBJECT - ANDRIAN JULIANSYAH
//=============================================================================

function arrayToObject(arr) {
   
    if (arr.length <= 0) {
      return console.log("")
    }
  
    for (var i = 0; i < arr.length; i++) {
      var newObject = {}
  
      var birthYear = arr[i][3];
      var now = new Date().getFullYear()
      var newAge;
  
      if (birthYear && now - birthYear > 0) {
        newAge = now - birthYear
      } else {
        newAge = 'Invalid Birth Year'
      }
  
      newObject.firstName = arr[i][0]
      newObject.lastName = arr[i][1]
      newObject.gender = arr[i][2]
      newObject.age = newAge
  
      var consoleText = (i + 1) + '. ' + newObject.firstName + ' ' + newObject.lastName + ': '
  
      console.log(consoleText)
      console.log(newObject)
    }
  }
  
  // Patokan Kode
  console.log("Soal No. 1 (Array to Object)")
  console.log("=====================================")
  var person = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
  arrayToObject(person)
  
  var person2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
  arrayToObject(person2)
  
  // Handling Error
  arrayToObject([]) // ""
  console.log("=====================================")

//SOAL 2 - OBJECT SHOPPING TIME - ANDRIAN JULIANSYAH
//=============================================================================
function shoppingTime(memberId, money) {

    if (!memberId) {
      return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000) {
      return "Mohon maaf, uang tidak cukup"
    } else {
      var newObject = {}; 
      var moneyChange = money;
      var purchasedList = [];
  
      var sepatu = 'Sepatu Stacattu';
      var baju1 = 'Baju Zoro';
      var baju2 = 'Baju H&N';
      var jaket = 'Sweater Uniklooh';
      var casing = 'Casing Handphone';
  
      var check = 0;
  
      for (var i = 0; moneyChange >= 50000 && check == 0; i++) {
        if (moneyChange >= 1500000) {
          purchasedList.push(sepatu)
          moneyChange -= 1500000
        } else if (moneyChange >= 500000) {
          purchasedList.push(baju1)
          moneyChange -= 500000
        } else if (moneyChange >= 250000) {
          purchasedList.push(baju2)
          moneyChange -= 250000
        } else if (moneyChange >= 175000) {
          purchasedList.push(jaket)
          moneyChange -= 175000
        } else if (moneyChange >= 50000) {
  
          if (purchasedList.length != 0) {
            for (var j = 0; j <= purchasedList.length - 1; j++) {
              if (purchasedList[j] == casing) {
                check += 1
              }
            }
            if (check == 0) {
              purchasedList.push(casing)
              moneyChange -= 50000
            }
          } else {
            purchasedList.push(casing)
            moneyChange -= 50000
          }
  
        }
      }
  
      newObject.memberId = memberId
      newObject.money = money
      newObject.listPurchased = purchasedList
      newObject.changeMoney = moneyChange
  
      return newObject
    }
  
  }
  
  console.log("Soal No. 2 OBJECT (SHOPPING TIME)")
  console.log("=====================================")
  console.log(shoppingTime('1820RzKrnWn08', 2475000));

  console.log(shoppingTime('82Ku8Ma742', 170000));

  console.log(shoppingTime('', 2475000)); 
  console.log(shoppingTime('234JdhweRxa53', 15000)); 
  console.log(shoppingTime()); 
  console.log("=====================================")

//SOAL 3 - OBJECT (NAIK ANGKOT) - ANDRIAN JULIANSYAH
//=============================================================================
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrOutput = []
    //your code here
    if (arrPenumpang.length <= 0) {
      return []
    }
  
    for (var x = 0; x < arrPenumpang.length; x++) {
      var objOutput = {}
      // var arrOrang = arrPenumpang[i]
      var asal = arrPenumpang[x][1]
      var tujuan = arrPenumpang[x][2]
  
      var indexAsal;
      var indexTujuan;
  
      for (var y = 0; y < rute.length; y++) {
        if (rute[y] == asal) {
          indexAsal = y
        } else if (rute[y] == tujuan) {
          indexTujuan = y
        }
      }
  
      var bayar = (indexTujuan - indexAsal) * 2000
      // var bayar = Math.abs(indexTujuan - indexAsal) * 2000
  
      objOutput.penumpang = arrPenumpang[x][0]
      objOutput.from = asal
      objOutput.destination = tujuan
      objOutput.bayar = bayar
  
      arrOutput.push(objOutput)
    }
  
    return arrOutput
  }
  
  console.log("Soal No. 3 OBJECT (NAIK ANGKOT)")
  console.log("=====================================")
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

  console.log(naikAngkot([])); //[]
  console.log("=====================================")
  