import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
} from 'react-native'

export default class LoginScreen extends React.Component {
    render() {
        return (
            <View>
                <Image source={require('./images/logo.png')} style={{ width: 280, height: 80 }} />

                <TextInput style={styles.textinput} placeholder="Username"
                underlineColorAndroid={'transparent'} />
                <TextInput style={styles.textinput} placeholder="Password"
                underlineColorAndroid={'transparent'} />
                <TouchableOpacity style={styles.button}>
                    <Text style={styles.btntext}>LOGIN</Text>
                </TouchableOpacity>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    LoginScreen: {
        alignSelf: 'center',
    },
    header: {
        fontSize: 24,
        color: 'red',
        paddingBottom: 10,
        marginBottom: 40,
        borderBottomColor: '#199187',
        borderBottomWidth: 1,
    },
    textinput: {
        alignSelf: 'stretch',
        height: 40,
        marginBottom: 30,
        borderBottomColor: '#f8f8f8',
        borderBottomWidth: 1,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        width: 150,
        backgroundColor: '#003366',
        marginTop: 10,
        borderRadius: 25,
        marginLeft: 65,
    },
    btntext: {
        color: '#fff',
        fontWeight: 'bold',
    }
})