//SOAL 1 - ARROW FUNCTION - ANDRIAN JULIANSYAH
//=============================================================================

const golden = () => {
    console.log("this is golden!!")
  }
console.log("SOAL 1 - ARROW FUNCTION")
console.log("=====================================")
    golden()
console.log("=====================================")

//SOAL 2 - OBJECT LITERAL DI ES6 - ANDRIAN JULIANSYAH
//=============================================================================

const newFunction = (firstName, lastName) => {
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: () => {
        console.log(firstName + " " + lastName)
        // return
      }
    }
  }
  
  //body code
  console.log("SOAL 2 - OBJECT LITERAL DI ES6")
  console.log("=====================================")
  newFunction("William", "Imoh").fullName()
  console.log("=====================================")

//SOAL 3 - Destructuring - ANDRIAN JULIANSYAH
//=============================================================================
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const { firstName, lastName, destination, occupation, spell } = newObject
  // Body code
  console.log("SOAL 3 - Destructuring")
  console.log("=====================================")
  console.log(firstName, lastName, destination, occupation, spell)
  console.log("=====================================")

//SOAL 4 - Array Spreading - ANDRIAN JULIANSYAH
//=============================================================================
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// Kombinasi Const = west.concat(east)
const combined = [...west, ...east]
//Body Code
console.log("SOAL 4 - Array Spreading")
console.log("=====================================")
console.log(combined)
console.log("=====================================")

//SOAL 5 - Template Literals - ANDRIAN JULIANSYAH
//=============================================================================
const planet = "earth"
const view = "glass"
const before = `Lorem  ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
// Body Code
console.log("SOAL 5 - Template Literals")
console.log("=====================================")
console.log(before)
console.log("=====================================") 