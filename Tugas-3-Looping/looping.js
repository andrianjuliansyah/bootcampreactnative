//SOAL 1 - LOOPING WHILE - ANDRIAN JULIANSYAH

var i = 2;
var txtCoding = " - I love coding";
var txtMobDev = " - I will become a mobile developer";
console.log('LOOPING PERTAMA')
while (i < 20) {
  console.log(i + txtCoding)
  i += 2;
}
console.log(i + txtCoding)
console.log('LOOPING KEDUA')
while (i >= 2) {
  console.log(i + txtMobDev)
  i -= 2;
}


//SOAL 2 - LOOPING FOR - ANDRIAN JULIANSYAH

console.log("OUTPUT")
var txtSantai = " - Santai"
var txtberkualitas = " - Berkualitas"
var txtloveCoding = " - I Love Coding"
for (i = 1; i <= 20; i++) {
  if (i % 2 != 1) {
    console.log(i + txtberkualitas)
  } else if (i % 3 == 0) {
    console.log(i + txtloveCoding)
  } else {
    console.log(i + txtSantai)
  }
}


//SOAL 3 - LOOPING Membuat Persegi Panjang # - ANDRIAN JULIANSYAH

i = 1;
var j = 1;
var panjang = 8;
var lebar = 4;
var pagar = '';

while (j <= lebar) { // 9 <= 8
  while (i <= panjang) { // 1 <= 4
    pagar += '#';
    i++;
  }
  console.log(pagar); // ####
  pagar = ''; // ""
  i = 1; // ""
  j++;
}


//SOAL 4 - LOOPING Membuat Membuat Tangga - ANDRIAN JULIANSYAH

x = 1;
y = 1;
var base = 7;
var height = 7;
var fence = "";
for (x = 1; x <= height; x++) {
    for (y = 1; y <= x; y++) {
        fence += "#";
    }
    console.log(fence)
    fence = "";
}

//SOAL 5 - LOOPING Membuat Papan Catur - ANDRIAN JULIANSYAH
x = 1;
y = 1;
var long = 8;
var wide = 8;
var catur = "";
for (y = 1; y <= wide; y++) {
  if (y % 2 == 1) {
    for (x = 1; x <= long; x++) {
      if (x % 2 == 1) {
        catur += " "
      } else {
        catur += "#"
      }
    }
  } else {
    for (x = 1; x <= long; x++) {
      if (x % 2 == 1) {
        catur += "#"
      } else {
        catur += " "
      }
    }
  }
  console.log(catur);
  catur = "";
}