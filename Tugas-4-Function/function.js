// Soal 1 - FUNCTION - ANDRIAN JULIANSYAH
/*
function teriak() {
    return "Halo Sanbers!"
  }
  console.log(teriak()) // "Halo Sanbers!" 

// Soal 2 - FUNCTION Function Dengan Nama kalikan() - ANDRIAN JULIANSYAH
function kalikan(a, b) {
        return a * b
    }
    var num1 = 12
    var num2 = 4
    
    var perkalian = kalikan(num1, num2)
    console.log(perkalian) // 48
*/

// Soal 3 - FUNCTION Dengan Nama introduce() - ANDRIAN JULIANSYAH

function introduce(name, age, address, hobby) {
    var sentence = "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!"
    return sentence
  }
  
  var name = "Agus"
  var age = 30
  var address = "Jln. Malioboro, Yogyakarta"
  var hobby = "Gaming"
  
  var kenalan = introduce(name, age, address, hobby)
  console.log(kenalan) 