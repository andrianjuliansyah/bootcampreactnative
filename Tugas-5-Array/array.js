//SOAL 1 - ARRAY RANGE - ANDRIAN JULIANSYAH
//=============================================================================

function range(startNum, finishNum) {
    var rangeArray = [];
    // var rangeLength = Math.abs(startNum - finishNum) + 1;
  
    if (startNum > finishNum) {
      var rangeLength = startNum - finishNum + 1;
      for (var i = 0; i < rangeLength; i++) {
        rangeArray.push(startNum - i)
      }
    } else if (startNum < finishNum) {
      var rangeLength = finishNum - startNum + 1;
      for (var i = 0; i < rangeLength; i++) {
        rangeArray.push(startNum + i)
      }
    } else if (!startNum || !finishNum) {
      return -1
    }
    return rangeArray
  }
  
  console.log("SOAL 1 ARRAY RANGE")
  console.log("===========================")
  console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  console.log(range(1)) // -1
  console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
  console.log(range(54, 50)) // [54, 53, 52, 51, 50]
  console.log(range()) // -1 
  console.log("===========================")
  

//SOAL 2 - ARRAY RANGE WITH STEP- ANDRIAN JULIANSYAH
//=============================================================================
function rangeWithStep(startNum, finishNum, step) {
    var rangeArray = [];
  
    if (startNum > finishNum) {
      var currentNum = startNum;
      for (var i = 0; currentNum >= finishNum; i++) {
        rangeArray.push(currentNum)
        currentNum -= step
      }
    } else if (startNum < finishNum) {// start = 1; finish =10; step=2
      var currentNum = startNum;
      for (var i = 0; currentNum <= finishNum; i++) { // i =1
        rangeArray.push(currentNum) //3
        currentNum += step // 9 + 2 = 11
      }
    } else if (!startNum || !finishNum || !step) {
      return -1
    }
    return rangeArray
  }
  
  console.log("SOAL 2 ARRAY RANGE WITH STEP")
  console.log("=====================================")
  console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
  console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
  console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
  console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]
  console.log("=====================================")
  
//SOAL 3 - ARRAY SUM OF RANGE - ANDRIAN JULIANSYAH
//=============================================================================
function sum(startNum, finishNum, step) {
    var rangeArray = [];
    var distance;
  
    if (!step) {
      distance = 1
    } else {
      distance = step
    }
  
    if (startNum > finishNum) {
      var currentNum = startNum;
      for (var i = 0; currentNum >= finishNum; i++) {
        rangeArray.push(currentNum)
        currentNum -= distance
      }
    } else if (startNum < finishNum) {
      var currentNum = startNum;
      for (var i = 0; currentNum <= finishNum; i++) {
        rangeArray.push(currentNum)
        currentNum += distance
      }
    } else if (!startNum && !finishNum && !step) {
      return 0
    } else if (startNum) {
      return startNum
    }
  
    var total = 0;
    for (var i = 0; i < rangeArray.length; i++) {
      total = total + rangeArray[i]
    }
    return total
  }
  
  console.log("SOAL 3 ARRAY SUM OF RANGE")
  console.log("=====================================")
  console.log(sum(1, 10)) // 55
  console.log(sum(5, 50, 2)) // 621
  console.log(sum(15, 10)) // 75
  console.log(sum(20, 10, 2)) // 90
  console.log(sum(1)) // 1
  console.log(sum()) // 0 
  console.log("=====================================")

//SOAL 4 - ARRAY MULTI DIMENSI - ANDRIAN JULIANSYAH
//=============================================================================
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], // 0
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], // 1
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], // 2
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] // 3
  ];
  
  function dataHandling(data) {
    var dataLength = data.length
    for (var i = 0; i < dataLength; i++) {
      var id = 'Nomor ID: ' + data[i][0]
      var nama = 'Nama Lengkap: ' + data[i][1]
      var ttl = 'TTL: ' + data[i][2] + ' ' + data[i][3]
      var hobi = 'Hobi: ' + data[i][4]
      console.log(id)
      console.log(nama)
      console.log(ttl)
      console.log(hobi)
      console.log()
    }
  }
  
  // panggil data:
  console.log("SOAL 4 - ARRAY MULTI DIMENSI")
  console.log("=====================================")
  dataHandling(input);
  console.log("=====================================")

//SOAL 5 - ARRAY BALIK KATA - ANDRIAN JULIANSYAH
//=============================================================================

function balikKata(kata) {
    // var oldKata = kata
    var newKata = '';
    for (var i = kata.length - 1; i >= 0; i--) { // length =11; index = 10
      newKata += kata[i]
    }
    return newKata;
  }
  
  console.log("SOAL 5 - ARRAY BALIK KATA")
  console.log("=====================================")
  console.log(balikKata("Kasur Rusak")) // kasuR rusaK
  console.log(balikKata("SanberCode")) // edoCrebnaS
  console.log(balikKata("Haji Ijah")) // hajI ijaH
  console.log(balikKata("racecar")) // racecar
  console.log(balikKata("I am Sanbers")) // srebnaS ma I 
  console.log("=====================================")


//SOAL 6 - ARRAY METODE ARRAY - ANDRIAN JULIANSYAH
//=============================================================================
console.log("SOAL 6 - ARRAY METODE ARRAY")
console.log("=====================================")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
handleData(input);

function handleData(data) {
  var dataBaru = data

  var newName = data[1] + 'Elsharawy' // Roman Alamsyah Elsharawy
  var newProvince = 'Provinsi ' + data[2] // Provinsi Bandar Lampung
  var gender = 'Pria'
  var institution = 'SMA Internasional Metro'

  dataBaru.splice(1, 1, newName)
  dataBaru.splice(2, 1, newProvince)
  dataBaru.splice(4, 1, gender, institution)

  var arrDate = data[3]
  var newDate = arrDate.split('/') // ['21', '05', '1989' ]
  var monthNum = newDate[1]
  var monthName = ''

  switch (monthNum) {
    case '01':
      monthName = 'Januari';
      break;
    case '02':
      monthName = 'Februari';
      break;
    case '03':
      monthName = 'Maret';
      break;
    case '04':
      monthName = 'April';
      break;
    case '05':
      monthName = 'Mei';
      break;
    case '06':
      monthName = 'Juni';
      break;
    case '07':
      monthName = 'Juli';
      break;
    case '08':
      monthName = 'Agustus';
      break;
    case '09':
      monthName = 'September';
      break;
    case '10':
      monthName = 'Oktober';
      break;
    case '11':
      monthName = 'November';
      break;
    case '12':
      monthName = 'Desember';
      break;
    default:
      break;
  }

  var dateJoin = newDate.join('-')
  var dateArr = newDate.sort(function (value1, value2) { return value2 - value1 }) //desc
  var editName = newName.slice(0, 15)

  console.log(dataBaru) // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
  console.log(monthName) // Mei
  console.log(dateArr) // ['1989', '21', '05']
  console.log(dateJoin) // 21-05-1989
  console.log(editName) //Roman Alamsyah
}
console.log("=====================================")
