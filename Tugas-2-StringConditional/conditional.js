// CONDITIONAL IF - ELSE Andrian Juliansyah
//============================================================================================
// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"

//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"

//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."

//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"

console.log('Output Soal If-Else 1')
console.log('===========================');
var nama = ""
var peran = ""

if (nama == '') {
  console.log('Nama harus diisi!')
} else if (nama && peran == '') {
  console.log('Halo ' + nama + ', Pilih Peranmu untuk memulai game')
} else if (nama == 'Jane' && peran == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
  console.log(`Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`)
}
console.log('===========================');

console.log('Output Soal If-Else 2')
console.log('===========================');
var nama = "John"
var peran = ""

if (nama == '') {
  console.log('Nama harus diisi!')
} else if (nama && peran == '') {
  console.log('Halo ' + nama + ', Pilih Peranmu untuk memulai game')
} else if (nama == 'Jane' && peran == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
  console.log(`Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`)
}
console.log('===========================');

console.log('Output Soal If-Else 3')
console.log('===========================');
var nama = "Jane"
var peran = "Penyihir"

if (nama == '') {
  console.log('Nama harus diisi!')
} else if (nama && peran == '') {
  console.log('Halo ' + nama + ', Pilih Peranmu untuk memulai game')
} else if (nama == 'Jane' && peran == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
  console.log(`Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`)
}
console.log('===========================');

console.log('Output Soal If-Else 4')
console.log('===========================');
var nama = "Jenita"
var peran = "Guard"

if (nama == '') {
  console.log('Nama harus diisi!')
} else if (nama && peran == '') {
  console.log('Halo ' + nama + ', Pilih Peranmu untuk memulai game')
} else if (nama == 'Jane' && peran == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
  console.log(`Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`)
}
console.log('===========================');

console.log('Output Soal If-Else 5')
console.log('===========================');
var nama = "Junaedi"
var peran = "Werewolf"

if (nama == '') {
  console.log('Nama harus diisi!')
} else if (nama && peran == '') {
  console.log('Halo ' + nama + ', Pilih Peranmu untuk memulai game')
} else if (nama == 'Jane' && peran == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, Jane\nHalo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama == 'Jenita' && peran == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, Jenita\nHalo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama == 'Junaedi' && peran == 'Werewolf') {
  console.log(`Selamat datang di Dunia Werewolf, Junaedi\nHalo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!`)
}
console.log('===========================');

//============================================================================================
// Soal Switch Case - Andrian Juliansyah
console.log('Output Soal Switch Case')
console.log('===========================');
var tanggal = 13; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 7; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1992; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
var teksBulan;

switch (true) {
  case (tanggal < 1 || tanggal > 31): {
    console.log('Input tanggal salah')
    break;
  }
  case (tahun < 1900 || tahun > 2200): {
    console.log('Input Tahun Salah')
    break;
  }
  case (bulan > 12 || bulan < 1):
    console.log('Input Bulan Salah')
    break;
  default:
    {
      switch (true) {
        case bulan == 1:
          teksBulan = 'Januari';
          break;
        case bulan == 2:
          teksBulan = 'Februari';
          break;
        case bulan == 3:
          teksBulan = 'Maret';
          break;
        case bulan == 4:
          teksBulan = 'April';
          break;
        case bulan == 5:
          teksBulan = 'Mei';
          break;
        case bulan == 6:
          teksBulan = 'Juni';
          break;
        case bulan == 7:
          teksBulan = 'Juli';
          break;
        case bulan == 8:
          teksBulan = 'Agustus';
          break;
        case bulan == 9:
          teksBulan = 'September';
          break;
        case bulan == 10:
          teksBulan = 'Oktober';
          break;
        case bulan == 11:
          teksBulan = 'November';
          break;
        case bulan == 12:
          teksBulan = 'Desember';
          break;
        default:
          break;
      }
      console.log(tanggal, ' ', teksBulan, ' ', tahun)
      break;
    }
}
console.log('===========================');
